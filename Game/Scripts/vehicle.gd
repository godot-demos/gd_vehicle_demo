
extends VehicleBody

var _space_just_pressed
var _space_just_released

var _action_left
var _action_right
var _action_up
var _action_down
var _action_space

var abs_speed = 0

func _ready():
	set_process(true)

func _process(dt):
	if _action_up:
		set_engine_force(100)

	if _action_down:
		set_engine_force(-100)

	if _action_left:
		set_steering(max(-0.5, get_steering() - 0.1))
	
	if _action_right:
		set_steering(min(0.5, get_steering() + 0.1))