
extends Node

var object

var connection
var peerstream

var x_space = "ui_accept"
var x_left = "ui_left"
var x_right = "ui_right"
var x_up = "ui_up"
var x_down = "ui_down"

func _ready():
	object = get_parent()
	set_process(true)
	#object.controller = self

func _process(dt):
	HandleInput()

func _on_ID_updated():
	#var id = str(character.id)
	var id = "ui"
	x_space = id + "_space"
	x_left = id + "_left"
	x_right = id + "_right"
	x_up = id + "_up"
	x_down = id + "_down"

func HandleInput():
	object._space_just_pressed = not object._action_space and Input.is_action_pressed(x_space)
	object._space_just_released = object._action_space and not Input.is_action_pressed(x_space)
	
	object._action_left = Input.is_action_pressed(x_left)
	object._action_right = Input.is_action_pressed(x_right)
	object._action_up = Input.is_action_pressed(x_up)
	object._action_down = Input.is_action_pressed(x_down)
	object._action_space = Input.is_action_pressed(x_space)

